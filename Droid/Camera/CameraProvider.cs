﻿using Xamarin.Forms;
using VictorFinneyC2itChallenge.Droid;

[assembly: Dependency(typeof(CameraProvider))]
namespace VictorFinneyC2itChallenge.Droid
	{
		using System;
		using System.Threading.Tasks;

		using Android.App;
		using Android.Content;
		using Android.Provider;

		using VictorFinneyC2itChallenge;

		using Java.IO;

		using Environment = Android.OS.Environment;
		using Uri = Android.Net.Uri;

		public class CameraProvider : ICameraProvider
		{
			private static File file;
			private static File pictureDirectory;

			private static TaskCompletionSource<Photo> tcs;

			public Task<Photo> TakePictureAsync()
			{
				Intent intent = new Intent(MediaStore.ActionImageCapture);

				pictureDirectory = new File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "CameraAppDemo");

				if (!pictureDirectory.Exists())
				{
					pictureDirectory.Mkdirs();
				}

				file = new File(pictureDirectory, String.Format("photo_{0}.jpg", Guid.NewGuid()));

				intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(file));

				var activity = (Activity)Forms.Context;
				activity.StartActivityForResult(intent, 0);

				tcs = new TaskCompletionSource<Photo>();

				return tcs.Task;
			}

			public static void OnResult(Result resultCode)
			{
				if (resultCode == Result.Canceled)
				{
					tcs.TrySetResult(null);
					return;
				}

				if (resultCode != Result.Ok)
				{
					tcs.TrySetException(new Exception("Unexpected error"));
					return;
				}

				Photo res = new Photo();
				res.Image = ImageSource.FromFile(file.Path);
				res.PhotoFilePath = file.Path;

				tcs.TrySetResult(res);
			}
		}
	}

