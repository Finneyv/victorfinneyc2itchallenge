﻿using System;
namespace VictorFinneyC2itChallenge
{
	public class Report
	{
		public Guid ReportGuid { get; set;}
		public DateTime CreatedAt { get; set;}
		public string RentalAgreementNumber { get; set;}
		public string CustomerName { get; set;}
		public string CustomerEmail { get; set;}
		public string LicensePlateNumber { get; set;}
	}
}

