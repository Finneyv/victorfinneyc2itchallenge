﻿using System;
using Xamarin.Forms;

namespace VictorFinneyC2itChallenge
{
	public class Photo
	{
		public Guid ReportGuid { get; set;}
		public Guid PhotoGuid { get; set;}
		public ImageSource Image { get; set;}
		public string PhotoFilePath { get; set;}
	}
}

