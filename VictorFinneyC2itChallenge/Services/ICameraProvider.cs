﻿using System;
using System.Threading.Tasks;

namespace VictorFinneyC2itChallenge
{
	public interface ICameraProvider
	{
		Task<Photo> TakePictureAsync();
	}
}

