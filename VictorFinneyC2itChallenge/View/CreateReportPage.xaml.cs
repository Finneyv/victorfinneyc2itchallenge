﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.IO;

using Xamarin.Forms;

namespace VictorFinneyC2itChallenge
{
	public partial class CreateReportPage : ContentPage
	{
		public Report report = new Report();
		public List<Photo> photos = new List<Photo>();
		public CreateReportPage()
		{
			InitializeComponent();
		}

		public async void OnAddPhoto(object o, EventArgs e){
			var cameraProvider = DependencyService.Get<ICameraProvider>();
			var pictureresult = await cameraProvider.TakePictureAsync();
			if (pictureresult != null) { 
				
			}
		}

		public async void OnSubmitReport(object o, EventArgs e) {
			bool passed = true;
			passed = EmailValidation(passed);
			passed = NumericValidation(passed);
			string submittedReport = $"Rental Agreement Number: {RentalAgreementNumber.Text} Customer Name: {CustomerName.Text} Customer Email: {CustomerEmail.Text} Plate Number: {PlateNumber.Text}";
			if (passed)
			{
				var userInput = await DisplayAlert("Do you want to submit a report with the following values?", submittedReport, "OK", "Cancel");
				if (userInput) {
					await Navigation.PopAsync();
				}
			}
			else { passed = true;}
		}

		public bool EmailValidation(bool passed)
		{
			bool pass = passed;
			if (CustomerEmail.Text == null)
			{
				pass = false;
				CustomerEmailValidator.Text = "Customer Email is Required";
			}
			else if (!Regex.Match(CustomerEmail.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
				{
					pass = false;
					CustomerEmailValidator.Text = "Customer Email must be an email";
				}
			return pass;
		}

		public bool NumericValidation(bool passed) {
			bool pass = passed;
			int parse;
			if (RentalAgreementNumber.Text == null)
			{
				pass = false;
				RentalValidator.Text = "Rental Agreement Number is Required";
			}
			else if (!int.TryParse(RentalAgreementNumber.Text, out parse)){
				pass = false;
				RentalValidator.Text = "Rental Agreement Number must be numeric";
			}
			return pass;
		}

		public void OnCancelReport(object o, EventArgs e) {
			Navigation.PopAsync();
		}
	}
}

