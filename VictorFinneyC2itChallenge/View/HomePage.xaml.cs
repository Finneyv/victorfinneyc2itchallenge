﻿using System;
using Xamarin.Forms;

namespace VictorFinneyC2itChallenge
{
	public partial class HomePage : ContentPage
	{
		public HomePage()
		{
			InitializeComponent();
		}
		public void OnFindReport(object o, EventArgs e) {
			Device.OpenUri(new Uri("https://www.google.com/"));
		}

		public void OnCreateReport(object o, EventArgs e) {
			Navigation.PushAsync(new CreateReportPage());
		}
	}
}

