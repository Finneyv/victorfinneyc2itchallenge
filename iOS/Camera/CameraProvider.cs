﻿using Xamarin.Forms;
using VictorFinneyC2itChallenge.iOS;

[assembly: Dependency(typeof(CameraProvider))]
namespace VictorFinneyC2itChallenge.iOS
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using UIKit;
	using Foundation;

	using VictorFinneyC2itChallenge;

	public class CameraProvider : ICameraProvider
	{
		public Task<Photo> TakePictureAsync() {
			var tcs = new TaskCompletionSource<Photo>();
			Camera.TakePicture(
				UIApplication.SharedApplication.KeyWindow.RootViewController,
				(imagePickerResult) =>
				{
					if (imagePickerResult == null)
					{
						tcs.TrySetResult(null);
						return;
					}
					Guid photoId = Guid.NewGuid();
					var photo = imagePickerResult.ValueForKey(new NSString("UIImagePickerControllerOriginalImage")) as UIImage;
					var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					string jpgFilename = Path.Combine(documentsDirectory, photoId + ".jpg");
					NSData imgData = photo.AsJPEG();
					NSError err = null;

					if (imgData.Save(jpgFilename, false, out err))
					{
						Photo result = new Photo();
						result.Image = ImageSource.FromStream(imgData.AsStream);
						result.PhotoFilePath = jpgFilename;
						result.PhotoGuid = photoId;

						tcs.TrySetResult(result);
					}
					else
					{
						tcs.TrySetException(new Exception(err.LocalizedDescription));
					}
				});
			return tcs.Task;
		}
	}
}

